import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
gambars:any=[];
news:any=[];
soons:any=[];
challs:any=[];

constructor(){
  this.gambars = [
    {nama:"assets/img/editor1.jpg", judul:"LOVEPHOBIA",detail:"Orang bilang cinta itu berwarna merah muda"},
    {nama:"assets/img/editor2.jpg", judul:"DILAN 1990",detail:"kisah cinta dengan latar belakang berbeda"},
    {nama:"assets/img/editor3.jpg", judul:"COMICONNECT TOUR",detail:"ambil tiket gratis hari ini "}
    ];
   
  this.news = [
    {nama:"assets/img/new1.jpg", genre:"action",judul:"KALA"},
    {nama:"assets/img/new2.jpg", genre:"comedy",judul:"SI NOPAL"},
    {nama:"assets/img/new3.jpg", genre:"comedy",judul:"SOEKIRMAN"},
    {nama:"assets/img/new4.jpg", genre:"action",judul:"ROCK"},
    {nama:"assets/img/new5.jpg", genre:"horor",judul:"HITAM"},
    {nama:"assets/img/new6.jpg", genre:"comedy",judul:"SUPERO"},
    {nama:"assets/img/new7.jpg", genre:"action",judul:"KALA"},
    {nama:"assets/img/new8.jpg", genre:"comedy",judul:"KELUARGA"},
    {nama:"assets/img/new9.jpg", genre:"slice",judul:"SIBLING"},
    {nama:"assets/img/new10.jpg", genre:"slice",judul:"INDONESIA"}
    ];  
  this.soons = [
    {nama:"assets/img/commingsoon2.jpg",genre:"comedy",judul:"SOUL ",tanggal:"15 NOV 17:00"},
    {nama:"assets/img/commingsoon2.jpg",genre:"action",judul:"MY EMO",tanggal:"24 NOV 17:00"},
    {nama:"assets/img/commingsoon3.jpg",genre:"action",judul:"CLASH",tanggal:"20 NOV 18:00"}
    ]; 
  this.challs = [
    {nama:"assets/img/challange1.jpg",judul:"Far Away",pengarang:"NECO-TEA",likes:"100"},
    {nama:"assets/img/challange2.jpg",judul:"31 DES'",pengarang:"Fandi Ilhami",likes:"164"},
    {nama:"assets/img/challange3.jpg",judul:"Surat No.63",pengarang:"awabara",likes:"169"},
    {nama:"assets/img/challange4.jpg",judul:"Chorus",pengarang:"Simon Munes",likes:"976"},
    {nama:"assets/img/challange5.jpg",judul:"BERLIKU-LIKU",pengarang:"KOSAKATA",likes:"29"}
    ];    
  } 
}